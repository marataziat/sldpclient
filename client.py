# https://softvelum.com/sldp/synced_demo/
from enum import Enum
import json
import websocket

class DataTypes(Enum):
    WEB_AAC_SEQUENCE_HEADER = 0
    WEB_AVC_SEQUENCE_HEADER = 2
    WEB_HEVC_SEQUENCE_HEADER = 5
    WEB_HEVC_FRAME = 7
    WEB_AVC_FRAME = 4
    WEB_AVC_KEY_FRAME = 3
    WEB_HEVC_KEY_FRAME = 6
    WEB_AAC_FRAME = 1

datatype_headers = [
    DataTypes.WEB_AAC_SEQUENCE_HEADER.value,
    DataTypes.WEB_AVC_SEQUENCE_HEADER.value,
    DataTypes.WEB_HEVC_SEQUENCE_HEADER.value
]

datatype_frames = [
    DataTypes.WEB_HEVC_FRAME.value,
    DataTypes.WEB_AVC_FRAME.value,
    DataTypes.WEB_AVC_KEY_FRAME.value,
    DataTypes.WEB_HEVC_KEY_FRAME.value
]

try:
    import os
    os.remove("test.ts")
except OSError:
    pass

file = open("test123.mp4" , "wb")

def on_message(ws, message):
    c = 10
    datatype = message[1]
    trackId = message[0] # a = s[0]
    l = len(message)

    if datatype in datatype_headers:
        # if datatype==0, it's audio codec
        #header = message[2:l]
        #file.write(header)
        pass
    if datatype in datatype_frames:
        frame = message[c+4:l]
        file.write(frame)

    if datatype == DataTypes.WEB_AAC_FRAME.value:
        frame = message[c+8+4:l]
        #file.write(frame)
        #print(f"something different {datatype}")

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    ws.send(
        json.dumps(
            {"command":"Play","streams":[{"type":"video","offset":"4000","steady":True,"stream":"video_ads/stream","sn":0},{"type":"audio","offset":"4000","steady":True,"stream":"video_ads/stream","sn":1}]}
        )
    )

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("wss://vd1.wmspanel.com/video_demo_without_ads/stream",
                              on_open = on_open,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close,
                              subprotocols=["sldp.softvelum.com"])

    ws.run_forever(origin='https://softvelum.com')